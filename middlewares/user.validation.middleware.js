const { user } = require('../models/user');
const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    let requiredFields = Object.keys(user)
    let newUserFields = Object.keys(req.body)
    requiredFields.shift()

    if (req.body.hasOwnProperty('id')) {
        req.error = true
        req.errorMessage = `Id can not be in request`
        req.erStatus = 400
    }

    if (req.body.firstName.length === 0) {
        req.error = true
        req.errorMessage = `firstName can not be empty`
        req.erStatus = 400
    }

    if (req.body.lastName.length === 0) {
        req.error = true
        req.errorMessage = `lastName can not be empty`
        req.erStatus = 400
    }


    if (isEmailInvalid(req.body.email)) {
        req.error = true
        req.errorMessage = `Email invalid. Only @gmail.com mails`
        req.erStatus = 400
    }


    if (isPhoneInvalid(req.body.phoneNumber)) {
        req.error = true
        req.errorMessage = `Phone invalid. Must be +380xxxxxxxx`
        req.erStatus = 400
    }

    if (isPasswordInvalid(req.body.password)) {
        req.error = true
        req.errorMessage = `Password to short. Minimum 3 symbols`
        req.erStatus = 400
    }

    let isAllFields = requiredFields.every(el => newUserFields.indexOf(el) !== -1)
    let fields = requiredFields.map(el => {
        if (!newUserFields.includes(el)) {
            return el
        }
    })
    fields = fields.filter(n => n).join(', ')

    if (!isAllFields) {
        req.error = true
        req.errorMessage = `This fields are required ${fields}`
        req.erStatus = 400
    }

    if (newUserFields.length > requiredFields.length) {
        let otherFields = newUserFields.map(el => {
            if (!requiredFields.includes(el)) {
                return el
            }
        })
        otherFields = otherFields.filter(n => n).join(', ')

        req.error = true
        req.errorMessage = `To much fields for new user. Wrong fields ${otherFields}`
        req.erStatus = 400
    }

    req.successStatus = 200
    req.successMessage = 'New user created'
    next();
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    let modelFields = Object.keys(user)
    let userUpdateFields = Object.keys(req.body)
    modelFields.shift()

    if (req.body.hasOwnProperty('id')) {
        req.error = true
        req.errorMessage = `Id can not be in request`
        req.erStatus = 400
    }

    if (req.body.firstName === '') {
        req.error = true
        req.errorMessage = `firstName can not be empty`
        req.erStatus = 400
    }

    if (req.body.lastName === '') {
        req.error = true
        req.errorMessage = `lastName can not be empty`
        req.erStatus = 400
    }


    if (req.body.email && isEmailInvalid(req.body.email)) {
        req.error = true
        req.errorMessage = `Email invalid. Only @gmail.com mails`
        req.erStatus = 400
    }


    if (req.body.phoneNumber && isPhoneInvalid(req.body.phoneNumber)) {
        req.error = true
        req.errorMessage = `Phone invalid. Must be +380xxxxxxxx`
        req.erStatus = 400
    }

    if (isPasswordInvalid(req.body.password)) {
        req.error = true
        req.errorMessage = `Password to short. Minimum 3 symbols`
        req.erStatus = 400
    }

    let isFieldsFromModel = userUpdateFields.every(el => modelFields.indexOf(el) !== -1)
    // console.log("isFieldsFromModel", isFieldsFromModel)
    let wrongFields = userUpdateFields.map(el => {
        if (!modelFields.includes(el)) {
            return el
        }
    })

    wrongFields = wrongFields.filter(n => n).join(', ')

    if (!isFieldsFromModel) {
        req.error = true
        req.errorMessage = `Wrong update field ${wrongFields}. User model without this field`
        req.erStatus = 404
    }

    if (userUpdateFields.length === 0) {
        req.error = true
        req.errorMessage = "Update object can not be empty"
        req.erStatus = 404
    }

    req.successStatus = 200
    req.successMessage = `Update current user with id ${req.params.id}`
    next();
}

function isEmailInvalid(email) {
    let validEmailTemplate = new RegExp("[a-zA-Z0-9]+@gmail.com")
    return !validEmailTemplate.test(email)
}
function isPhoneInvalid(phoneNumber) {
    let validPhoneNumber = new RegExp("^\\+380[0-9]{9}$")
    return !validPhoneNumber.test(phoneNumber)
}
function isPasswordInvalid(pass) {
    return (pass && pass.length < 3)
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;