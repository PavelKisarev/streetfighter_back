const responseMiddleware = (req, res, next) => {
    // TODO: Implement middleware that returns result of the query
    if (req.error) {
        return res.status(req.erStatus).json({
            "error": req.error,
            "message": req.errorMessage
        })
    }
    else {

        next()
    }
}

exports.responseMiddleware = responseMiddleware;