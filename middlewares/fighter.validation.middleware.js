const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    let requiredFields = Object.keys(fighter)
    let newFighterFields = Object.keys(req.body)

    requiredFields = requiredFields.filter(el => (el != 'id' && el != 'health'))

    if (req.body.hasOwnProperty('id')) {
        req.error = true
        req.errorMessage = `Id can not be in request`
        req.erStatus = 400
    }

    if (isNameInvalid(req.body.name)) {
        req.error = true
        req.errorMessage = `Name is required and can not be empty`
        req.erStatus = 400
    }

    if (isPowerInvalid(req.body.power)) {
        req.error = true
        req.errorMessage = `Power is required and must be 1 < power < 100`
        req.erStatus = 400
    }

    if (isDefenseInvalid(req.body.defense)) {
        req.error = true
        req.errorMessage = `Defense is required and must be 1 < defense < 10`
        req.erStatus = 400
    }

    if (!req.body.hasOwnProperty('health')) {
        req.body.health = 100
    }

    else {
        requiredFields.push('health')
        if (isHealthInvalid(req.body.health)) {
            req.error = true
            req.errorMessage = `Wrong health. Must be 80 < health < 120`
            req.erStatus = 400
        }
    }


    let isAllFields = requiredFields.every(el => newFighterFields.indexOf(el) !== -1)

    let fields = requiredFields.map(el => {
        if (!newFighterFields.includes(el)) {
            return el
        }
    })
    fields = fields.filter(n => n).join(', ')

    if (!isAllFields) {
        req.error = true
        req.errorMessage = `This fields are required ${fields}`
        req.erStatus = 400
    }

    if (newFighterFields.length > requiredFields.length) {
        let otherFields = newFighterFields.map(el => {
            if (!requiredFields.includes(el)) {
                return el
            }
        })
        otherFields = otherFields.filter(n => n).join(', ')

        if (!(otherFields === 'health')) {
            req.error = true
            req.errorMessage = `To much fields for new fighter. Wrong fields ${otherFields}`
            req.erStatus = 400
        }
    }



    req.successStatus = 200
    req.successMessage = 'New fighter created'
    next();
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    let modelFields = Object.keys(fighter)
    let fighterUpdateFields = Object.keys(req.body)
    modelFields.shift()


    if (isNameInvalid(req.body.name)) {
        req.error = true
        req.errorMessage = `Name is required and can not be empty`
        req.erStatus = 400
    }

    if (isPowerInvalid(req.body.power)) {
        req.error = true
        req.errorMessage = `Power is required and must be 1 < power < 100`
        req.erStatus = 400
    }

    if (isDefenseInvalid(req.body.defense)) {
        req.error = true
        req.errorMessage = `Defense is required and must be 1 < defense < 10`
        req.erStatus = 400
    }

    if (isHealthInvalid(req.body.health)) {
        req.error = true
        req.errorMessage = `Wrong health. Must be 80 < health < 120`
        req.erStatus = 400
    }

    let isFieldsFromModel = fighterUpdateFields.every(el => modelFields.indexOf(el) !== -1)

    let wrongFields = fighterUpdateFields.map(el => {
        if (!modelFields.includes(el)) {
            return el
        }
    })

    wrongFields = wrongFields.filter(n => n).join(', ')

    if (!isFieldsFromModel) {
        req.error = true
        req.errorMessage = `Wrong update field ${wrongFields}. Fighter model without this field`
        req.erStatus = 404
    }

    if (fighterUpdateFields.length === 0) {
        req.error = true
        req.errorMessage = "Update object can not be empty"
        req.erStatus = 404
    }

    req.successStatus = 200
    req.successMessage = `Update current fighter with id ${req.params.id}`
    next();
}

function isDefenseInvalid(defense) {
    return !(defense > 1 && defense < 10)
}
function isPowerInvalid(power) {
    return !(power > 1 && power < 100)
}
function isHealthInvalid(health) {
    return !(health > 80 && health < 120)
}
function isNameInvalid(name) {
    return name === ''
}


exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;