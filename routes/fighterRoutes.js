const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter

router.get('/', (req, res) => {
    const fightersAll = FighterService.getAll()
    if (fightersAll.length === 0) {
        return res.status(404).json({ 'error': true, 'message': `No fighters in database` })
    }
    return res.status(200).json(fightersAll)
})


router.get('/:id', (req, res) => {
    const currentFighterId = req.params.id
    const currentFighter = FighterService.getOne(currentFighterId)
    if (!currentFighter) {
        return res.status(404).json({ 'error': true, 'message': `User with current id ${currentFighterId} does not exist` })
    }
    return res.status(200).json(currentFighter)
})

router.post('/', createFighterValid, responseMiddleware, (req, res) => {
    const newFighter = { ...req.body }
    if (!FighterService.search({ "name": req.body.name })) {

        FighterService.create(newFighter)
        return res.status(req.successStatus).json({ 'message': req.successMessage })

    }
    return res.status(400).json({
        "error": true,
        "message": `Fighter with this name - ${req.body.name} already exist`
    })
})

router.put('/:id', updateFighterValid, responseMiddleware, (req, res) => {
    FighterService.update(req.params.id, { ...req.body })
    return res.status(req.successStatus).json({ 'message': req.successMessage })
})

router.delete('/:id', (req, res) => {
    FighterService.delete(req.params.id)
    return res.json({
        'message': `Delete current fighter with id ${req.params.id}`
    })
})

module.exports = router;