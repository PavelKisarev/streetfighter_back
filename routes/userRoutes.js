const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { UserRepository } = require('../repositories/userRepository');


const router = Router();

// router.use(createUserValid)

// TODO: Implement route controllers for user

router.get('/', (req, res) => {
    const usersAll = UserService.getAll()
    if (usersAll.length === 0) {
        return res.status(404).json({ 'error': true, 'message': `No users in database` })
    }
    return res.status(200).json(usersAll)
})


router.get('/:id', (req, res) => {
    const currentUserId = req.params.id
    const currentUser = UserService.getOne(currentUserId)
    if (!currentUser) {
        return res.status(404).json({ 'error': true, 'message': `User with current id ${currentUserId} does not exist` })
    }
    return res.status(200).json(currentUser)
})

router.post('/', createUserValid, responseMiddleware, (req, res) => {
    const newUser = { ...req.body }
    if (!UserService.search({ "email": req.body.email })) {
        if (!UserService.search({ "phoneNumber": req.body.phoneNumber })) {
            UserService.create(newUser)
            return res.status(req.successStatus).json({ 'message': req.successMessage })
        }
        return res.status(400).json({
            "error": true,
            "message": `User with this phone - ${req.body.phoneNumber} already exist`
        })
    }
    return res.status(400).json({
        "error": true,
        "message": `User with this email - ${req.body.email} already exist`
    })
})

router.put('/:id', updateUserValid, responseMiddleware, (req, res) => {
    UserService.update(req.params.id, { ...req.body })
    return res.status(req.successStatus).json({ 'message': req.successMessage })
})

router.delete('/:id', (req, res) => {
    UserService.delete(req.params.id)
    return res.json({
        'message': `Delete current user with id ${req.params.id}`
    })
})



module.exports = router;