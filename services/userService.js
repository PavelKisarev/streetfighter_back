const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user

    getAll() {
        const usersAll = UserRepository.getAll()
        return usersAll
    }

    getOne(id) {
        const currentUser = UserRepository.getOne(id)
        return currentUser
    }

    create(data) {
        UserRepository.create(data)
    }

    update(id, updateData) {
        UserRepository.update(id, updateData)
    }

    delete(id) {
        UserRepository.delete(id)
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();