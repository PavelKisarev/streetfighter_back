const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters

    getAll() {
        const usersAll = FighterRepository.getAll()
        return usersAll
    }

    getOne(id) {
        const currentUser = FighterRepository.getOne(id)
        return currentUser
    }

    create(data) {
        FighterRepository.create(data)
    }

    update(id, updateData) {
        FighterRepository.update(id, updateData)
    }

    delete(id) {
        FighterRepository.delete(id)
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }
}

module.exports = new FighterService();